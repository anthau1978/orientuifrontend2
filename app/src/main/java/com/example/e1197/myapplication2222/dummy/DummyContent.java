package com.example.e1197.myapplication2222.dummy;


import com.example.e1197.myapplication2222.Singleton1;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

  private static  int COUNT =5;

    static {
        // Add some sample items.
        Singleton1.initRoute(1);

         COUNT = Singleton1.jsonArray2.length();
        for (int i = 0; i < COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static DummyItem createDummyItem(int position) {

        String name="";
        int ID=0;
        try {
            name=Singleton1.jsonArray2.getJSONObject(position).getString("name");
            ID=Integer.parseInt(Singleton1.jsonArray2.getJSONObject(position).getString("ID"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        DummyItem item= new DummyItem(String.valueOf(position), name, makeDetails(position));
        try {
            item.id=Singleton1.jsonArray2.getJSONObject(position).getString("ID");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return item;
    }

    private static String makeDetails(int position) {
        String name="";
        String Length="";
        int ID=0;
        try {
             name=Singleton1.jsonArray2.getJSONObject(position).getString("name");
            Length="" + Singleton1.jsonArray2.getJSONObject(position).getString("length");
            ID=Integer.parseInt(Singleton1.jsonArray2.getJSONObject(position).getString("ID"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringBuilder builder = new StringBuilder();
        Singleton1.initRoute(ID);

        //Prints dara

        builder.append("\nRadan nimi=" + name );
        builder.append("\nRadan pituus=" + Length);
        builder.append("\nRadalla rasteja=" +  Singleton1.jsonArray.length() );


        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public String id;
        public final String content;
        public final String details;

        public DummyItem(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
