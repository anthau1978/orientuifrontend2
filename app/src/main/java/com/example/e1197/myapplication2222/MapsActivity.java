package com.example.e1197.myapplication2222;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.SignInButton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.graphics.Color.GREEN;

class Target implements View.OnClickListener {
    GoogleMap mMap;

    public Target(GoogleMap mMap)  {
        this.mMap=mMap;
    }
    @Override
    public void onClick(View v) {



        try {
            JSONObject item1 = Singleton1.jsonArray.getJSONObject(Singleton1.checked);
            LatLng spot = new LatLng(item1.getDouble("lat"), item1.getDouble("lon"));

            mMap.moveCamera(CameraUpdateFactory.newLatLng(spot));

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }
}

class GetUrl extends AsyncTask<String, Void, String> {
    int id;
    public GetUrl(int id)  {
        this.id=id;

    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... params) {

        return Singleton1.initRoute( Singleton1.listchecked);
    }

}

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LocationManager locationManager;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ActivityCompat.requestPermissions(MapsActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
          1);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);

       DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        try {
            menu.add("Nimi=" + Singleton1.jsonArray2.getJSONObject(Singleton1.listchecked).getString("name"));

            menu.add("Pituus=" + Singleton1.jsonArray2.getJSONObject(Singleton1.listchecked).getString("length"));

            Button button = (Button) findViewById(R.id.next);
            try {
                button.setText("Seuraava rasti: " + Singleton1.jsonArray.getJSONObject(Singleton1.checked).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

      //      menu.add("Rata=" + Singleton1.jsonArray.getJSONObject(Singleton1.checked).getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }





        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();




        /*
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        System.err.println("drawer=" + drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        */

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, 1);


        Singleton1.button = (Button) this.findViewById(R.id.button3);

        Singleton1.button.setVisibility(View.INVISIBLE);


    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        GetUrl url = new GetUrl(1);
        url.doInBackground("a");

        mMap = googleMap;
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        Target target=new Target(mMap);

        System.err.println("fab="+ fab);
        fab.setOnClickListener(target);


        mMap.setIndoorEnabled(true);

        mMap.setMapType(2);
        mMap.setIndoorEnabled(true);
        mMap.setMinZoomPreference(17);
        mMap.setMaxZoomPreference(17);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, 1);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


        }


        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Singleton1.manager = locationManager;
        Criteria criteria=new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);

        MarkerOptions cirle=new MarkerOptions();
        cirle.position(mMap.getCameraPosition().target);

        Singleton1.circle=mMap.addMarker(cirle);
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                Singleton1.circle.setPosition(mMap.getCameraPosition().target);
            }

        } );


        try {


            try {

               // Singleton1.button2.setText(Singleton1.jsonArray2.getJSONObject(0).getString("name").toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (int counter = 0; counter < Singleton1.jsonArray.length(); counter++) {

                JSONObject item1 = Singleton1.jsonArray.getJSONObject(counter);
                LatLng spot = new LatLng(item1.getDouble("lat"), item1.getDouble("lon"));

                CircleOptions cirleMark=new CircleOptions();
                cirleMark.center(spot);
                cirleMark.radius(10);
                cirleMark.fillColor(Color.TRANSPARENT);
                cirleMark.strokeColor(Color.RED);
                mMap.addCircle(cirleMark);



                MarkerOptions marker = new MarkerOptions();
                Bitmap.Config conf = Bitmap.Config.ARGB_8888;

                Bitmap bmp = Bitmap.createBitmap(300, 100, conf);
                Canvas canvas = new Canvas(bmp);
                Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                paint.setColor(Color.RED); // Text color
                paint.setTextSize(14 * 6); // Text size

                paint.setShadowLayer(1f, 0f, 1f, Color.WHITE); // Text shadow
                paint.setColor(Color.WHITE);
                Rect bounds = new Rect();
                String text = item1.getString("name");
                paint.getTextBounds(text, 0, text.length(), bounds);
                canvas.drawText(text, 0, 70, paint);
                marker.title(item1.getString("name") + counter);
                marker.position(spot);
                marker.flat(true);
                marker.icon(BitmapDescriptorFactory.fromBitmap(bmp));
                Marker marker1=mMap.addMarker(marker);

                Singleton1.markers.add(marker1);
                if(counter==Singleton1.checked) {
                    marker1.showInfoWindow();
                }
                System.err.println("Jes3J=" + "uu1");


            }
        } catch(Exception e)  {

        }



        //Update place
        locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria,true), 0, 0,
                new LocationListener() {

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onLocationChanged(Location location) {
                        if(Singleton1.jsonArray.length()>0  && Singleton1.checked< Singleton1.jsonArray.length() ) {
                            try {

                                JSONObject item = Singleton1.jsonArray.getJSONObject(Singleton1.checked);

                                if (item.getString("lat").length() > 4 && item.getString("lon").length() > 4) {
                                    String lat1 = item.getString("lat").substring(0, Singleton1.accuracy);
                                    String lon1 = item.getString("lon").substring(0, Singleton1.accuracy);

                                    String latGps = "" + location.getLatitude();
                                    String lonGps = "" + location.getLongitude();
                                    latGps = latGps.substring(0, Singleton1.accuracy);
                                    lonGps = lonGps.substring(0, Singleton1.accuracy);

                                    double flatGps = Double.parseDouble(latGps);
                                    double flonGps = Double.parseDouble(lonGps);

                                    double flat1 = Double.parseDouble(lat1);
                                    double flon1 = Double.parseDouble(lon1);

                                    //ollaan rastilla vaikesutasoa voidaan helpottaa. Nyt se on 20 metriä
                                    double dist = distance(flatGps, flonGps, flat1, flon1);

                                    if (distance(flatGps, flonGps, flat1, flon1) < 0.030) {
                                        Singleton1.button.setVisibility(View.VISIBLE);


                                    } else {
                                        Singleton1.button.setVisibility(View.INVISIBLE);

                                    }
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                });

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


        // Register the listener with the Location Manager to receive location updates


        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria,true));

        LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));





    }
    /*
        http://stackoverflow.com/questions/6981916/how-to-calculate-distance-between-two-locations-using-their-longitude-and-latitu
        */
    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public void check(View view) {
        try {
            Singleton1.markers.elementAt(Singleton1.checked).hideInfoWindow();
            Singleton1.checked++;
            Singleton1.markers.elementAt(Singleton1.checked).showInfoWindow();
            Button button = (Button) findViewById(R.id.next);
            try {
                button.setText("Seuraava rasti: " + Singleton1.jsonArray.getJSONObject(0).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch(Exception e) {
            //TODO Arrival to finish
            Singleton1.button.setText("Maali");
        }

    }
    public void addPlace(boolean method,String name)  {

        String url;

        String result = null;
        try {

            URL url1 = new URL("http://anthau.net:9080/WebApplication13/webresources/map.rastidesc");
            HttpURLConnection urlConnection = (HttpURLConnection) url1.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();


            OutputStream outputStream = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }

            Criteria criteria=new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            if(method) {

                Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria,true));

               // Location location = mMap.getMyLocation();


                writer.write("{\"desc1\":\"23\",\"id\":36,\"lat\":\"" + location.getLatitude() + "\",\"lon\":\"" + location.getLongitude() + "\",\"name\":\"" + name + "\" ,\"routeid\":" + Singleton1.listchecked +"}");
                CircleOptions cirleMark=new CircleOptions();

                LatLng target=new LatLng(location.getLatitude(),location.getLongitude());

                CircleOptions center = cirleMark.center(target);
                cirleMark.radius(10);
                cirleMark.fillColor(Color.TRANSPARENT);
                cirleMark.strokeColor(Color.RED);
                mMap.addCircle(cirleMark);
                /*
                CircleOptions cirleMark=new CircleOptions();
                LatLng target=new LatLng(location.getLatitude(),location.getLongitude());
                cirleMark.center(target);
                cirleMark.radius(10);
                cirleMark.fillColor(Color.TRANSPARENT);
                cirleMark.strokeColor(Color.RED);
                mMap.addCircle(cirleMark);

                MarkerOptions marker = new MarkerOptions();
                Bitmap.Config conf = Bitmap.Config.ARGB_8888;

                Bitmap bmp = Bitmap.createBitmap(300, 100, conf);
                Canvas canvas = new Canvas(bmp);
                Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                paint.setColor(Color.RED); // Text color
                paint.setTextSize(14 * 6); // Text size

                paint.setShadowLayer(1f, 0f, 1f, Color.WHITE); // Text shadow
                paint.setColor(Color.WHITE);
                Rect bounds = new Rect();
                String text = name;
                paint.getTextBounds(text, 0, text.length(), bounds);
                canvas.drawText(text, 0, 70, paint);
                marker.title(name);
                marker.position(new LatLng(location.getLatitude(),location.getLongitude()));
                marker.flat(true);
                marker.icon(BitmapDescriptorFactory.fromBitmap(bmp));
                Marker marker1=mMap.addMarker(marker);
                */
            }
            else  {

                writer.write("{\"desc1\":\"23\",\"id\":36,\"lat\":\"" + mMap.getCameraPosition().target.latitude + "\",\"lon\":\"" + mMap.getCameraPosition().target.longitude + "\",\"name\":\""+ name +"\",\"routeid\":" + Singleton1.listchecked+"}");
                CircleOptions cirleMark=new CircleOptions();

                CircleOptions center = cirleMark.center(mMap.getCameraPosition().target);
                cirleMark.radius(10);
                cirleMark.fillColor(Color.TRANSPARENT);
                cirleMark.strokeColor(Color.RED);
                mMap.addCircle(cirleMark);

            }

            writer.close();
            outputStream.close();


            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
            String line = null;
            StringBuilder sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();
            result = sb.toString();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void Showdata(View view) {

        //    Alert customdialog  = new Alert(this);






    }

    public void add(View view) {


        System.err.println("lisää");

        AlertDialog.Builder builder1 = new AlertDialog.Builder(view.getContext());
        builder1.setTitle("Valitse syöttötapa");


        EditText input1 = new EditText(this);
        input1.setInputType(InputType.TYPE_CLASS_TEXT);

        final EditText input = input1;

        String name = "";
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder1.setView(input);

        builder1.setMessage("Valitse syöttötapa gps tai kartta");

        builder1.setPositiveButton(
                "GPS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        addPlace(true,input.getText().toString());

                        dialog.cancel();

                    }
                });

        builder1.setNeutralButton(
                "Kartta",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addPlace(false,input.getText().toString());

                        dialog.cancel();
                    }
                });

        builder1.show();


        return ;




    }
    public void delete(View view) {


        Singleton1.button.setBackgroundColor(Color.BLUE);
    }
    public void AddRoute(View view) {



    }




}